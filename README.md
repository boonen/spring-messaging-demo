# Demo project to test features of Spring Messaging

## Batch processing of messages

The files `BatchDemoController` and `BatchMessageReceiver` demonstrate how Spring can fetch AMQP messages in batches and
store them using batch queries in a database. This mechanism is very efficient when high volumes of data need to be
processed and stored in a database. Failures are handled using 'retry queues' and 'dead letter queues' which are **not**
processed in batch in order to prevent messages from blocking one another. 

E.g. when a batch of 10 messages is read from the *event* queue and message number 6 contains corrupt data then all
messages are sent to the *event.wait* queue and processed one by one when the TTL has finished. Say that message 6
causes an unrecoverable error then it will fail eventually and end up in the *event.dlq* queue. The other 9 messages
will be processed correctly, though not in batches.

```plantuml
component EventProducer <<AMQP>> as producer
component BatchEventConsumer <<AMQP>> as batch_consumer
component EventConsumer <<AMQP>> as consumer
component EventArchiver <<JPA>> as archiver
queue event
queue event.wait as wait
queue event.retry as retry
queue event.dlq as dlq
database archive

producer -> event
event <-- batch_consumer : read batch of messages \n from queue
batch_consumer -> archiver : batch insert events \n in database
archiver -> archive : <<JDBC>>

batch_consumer --> wait : message caused \n an exception
wait ..> retry : retry message after TTL
retry <- consumer : read single message \n from queue
consumer -> archiver : insert single event \n in database
consumer --> wait : retry message \n when maximum retries \n are NOT exceeded
consumer --> dlq : put message on \n dead letter queue \n when maximum retries \n are exceeded
```