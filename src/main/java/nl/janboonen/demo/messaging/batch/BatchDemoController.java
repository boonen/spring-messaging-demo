package nl.janboonen.demo.messaging.batch;

import static nl.janboonen.demo.messaging.batch.BatchAmqpConfiguration.PRIMARY_ROUTING_KEY;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.janboonen.demo.messaging.shared.EventArchiveService;
import nl.janboonen.demo.messaging.shared.entity.EventArchiveEntry;
import nl.janboonen.demo.messaging.shared.model.BatchEvent;
import org.springframework.amqp.core.Exchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@AllArgsConstructor
@RestController
@RequestMapping("/api/v0/demo")
public class BatchDemoController {

  private final AtomicLong serialCounter = new AtomicLong();

  private final RabbitTemplate rabbitTemplate;

  private final EventArchiveService eventArchiveService;

  private final Exchange exchange;

  @PostMapping("/{batchId}")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  public void sendBatchEvents(@PathVariable String batchId,
      @RequestParam(defaultValue = "10") int batchSize, @RequestParam String username) {
    for (int i = 1; i <= batchSize; i++) {
      rabbitTemplate.convertAndSend(exchange.getName(), PRIMARY_ROUTING_KEY, createBatchEvent(batchId, username, i));
    }
    log.info("Sent {} messages to exchange '{}' using routing key '{}'.", batchSize, exchange.getName(), PRIMARY_ROUTING_KEY);
  }

  @GetMapping("/{batchId}")
  public List<EventArchiveEntry> getBatchEvents(@PathVariable String batchId) {
    return eventArchiveService.getAllEventsFromArchive(batchId);
  }

  private BatchEvent createBatchEvent(String batchId, String username, int messageNumber) {
    long serialNumber = serialCounter.incrementAndGet();
    log.trace("Generated serial number {} for next message.", serialNumber);
    return BatchEvent.builder()
        .batchId(batchId)
        .createdOn(ZonedDateTime.now())
        .serialNumber(serialNumber)
        .messageNumber(messageNumber)
        .username(username)
        .build();
  }

}
