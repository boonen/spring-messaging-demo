package nl.janboonen.demo.messaging.batch;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.QueueBuilder;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.boot.autoconfigure.amqp.SimpleRabbitListenerContainerFactoryConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BatchAmqpConfiguration {

  public static final String PRIMARY_ROUTING_KEY = "archive";

  public static final String ARCHIVE_QUEUE_NAME = "archive-queue";

  private static final String WAIT_QUEUE_NAME = ARCHIVE_QUEUE_NAME + ".wait";

  public static final String RETRY_QUEUE_NAME = ARCHIVE_QUEUE_NAME + ".retry";

  public static final String DEADLETTER_QUEUE_NAME = ARCHIVE_QUEUE_NAME + ".dlq";

  private static final String EXCHANGE_NAME = "jb-demo-exchange";

  @Bean
  TopicExchange exchange() {
    return new TopicExchange(EXCHANGE_NAME, true, false);
  }

  @Bean
  Queue archiveQueue() {
    return QueueBuilder.durable(ARCHIVE_QUEUE_NAME)
        .deadLetterExchange(EXCHANGE_NAME)
        .deadLetterRoutingKey(RETRY_QUEUE_NAME)
        .build();
  }

  @Bean
  Queue waitQueue() {
    return QueueBuilder.durable(WAIT_QUEUE_NAME)
        .deadLetterExchange(EXCHANGE_NAME)
        .deadLetterRoutingKey(WAIT_QUEUE_NAME)
        .ttl(10000)
        .build();
  }

  @Bean
  Queue retryQueue() {
    return QueueBuilder.durable(RETRY_QUEUE_NAME)
        .deadLetterExchange(EXCHANGE_NAME)
        .deadLetterRoutingKey(RETRY_QUEUE_NAME)
        .build();
  }

  @Bean
  Queue deadletterQueue() {
    return QueueBuilder.durable(DEADLETTER_QUEUE_NAME)
        .build();
  }

  @Bean
  Binding archiveQueueBinding() {
    return BindingBuilder
        .bind(archiveQueue())
        .to(exchange())
        .with(PRIMARY_ROUTING_KEY);
  }

  @Bean
  Binding waitQueueBinding() {
    return BindingBuilder
        .bind(waitQueue())
        .to(exchange())
        .with(WAIT_QUEUE_NAME);
  }

  @Bean
  Binding retryQueueBinding() {
    return BindingBuilder
        .bind(retryQueue())
        .to(exchange())
        .with(RETRY_QUEUE_NAME);
  }

  @Bean
  Binding dlqBinding() {
    return BindingBuilder
        .bind(deadletterQueue())
        .to(exchange())
        .with(DEADLETTER_QUEUE_NAME);
  }

  @Bean
  RabbitTemplate rabbitTemplate(final ConnectionFactory connectionFactory, final Jackson2JsonMessageConverter messageConverter) {
    final var rabbitTemplate = new RabbitTemplate(connectionFactory);
    rabbitTemplate.setMessageConverter(messageConverter);
    return rabbitTemplate;
  }

  @Bean
  Jackson2JsonMessageConverter messageConverter(ObjectMapper objectMapper) {
    objectMapper.registerModule(new JavaTimeModule());
    return new Jackson2JsonMessageConverter(objectMapper);
  }

  @Bean
  SimpleRabbitListenerContainerFactory rabbitListenerContainerFactory(final ConnectionFactory connectionFactory,
      final Jackson2JsonMessageConverter messageConverter, final SimpleRabbitListenerContainerFactoryConfigurer configurer) {
    SimpleRabbitListenerContainerFactory factory = new SimpleRabbitListenerContainerFactory();
    configurer.configure(factory, connectionFactory);
    factory.setMessageConverter(messageConverter);
    factory.setBatchListener(true);
    return factory;
  }

  @Bean
  SimpleRabbitListenerContainerFactory retryListenerContainerFactory(final ConnectionFactory connectionFactory,
      final Jackson2JsonMessageConverter messageConverter, final SimpleRabbitListenerContainerFactoryConfigurer configurer) {
    SimpleRabbitListenerContainerFactory factory = new SimpleRabbitListenerContainerFactory();
    configurer.configure(factory, connectionFactory);
    factory.setMessageConverter(messageConverter);
    factory.setBatchSize(1);
    factory.setDeBatchingEnabled(false);
    factory.setConsumerBatchEnabled(false);
    factory.setPrefetchCount(1);
    return factory;
  }

}
