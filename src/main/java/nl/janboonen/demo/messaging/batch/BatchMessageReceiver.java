package nl.janboonen.demo.messaging.batch;

import static nl.janboonen.demo.messaging.batch.BatchAmqpConfiguration.DEADLETTER_QUEUE_NAME;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.UUID;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import nl.janboonen.demo.messaging.shared.entity.EventArchiveEntry;
import nl.janboonen.demo.messaging.shared.entity.EventArchiveEntryRepository;
import nl.janboonen.demo.messaging.shared.model.BatchEvent;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitMessagingTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class BatchMessageReceiver {

  final Random random = new Random();

  final EventArchiveEntryRepository eventArchiveEntryRepository;

  final RabbitMessagingTemplate rabbitMessagingTemplate;

  @Value("${jbv.simulation.exception-probability:1.0}")
  double exceptionProbability;

  @Value("${jbv.simulation.exception-probability-failed-message:60.0}")
  double exceptionProbabilityFailedMessage;

  public BatchMessageReceiver(EventArchiveEntryRepository eventArchiveEntryRepository,
      RabbitMessagingTemplate rabbitMessagingTemplate) {
    this.eventArchiveEntryRepository = eventArchiveEntryRepository;
    this.rabbitMessagingTemplate = rabbitMessagingTemplate;
  }

  @RabbitListener(queues = BatchAmqpConfiguration.ARCHIVE_QUEUE_NAME)
  public void receiveMessage(final List<Message<BatchEvent>> messages) {
    Set<EventArchiveEntry> events = new HashSet<>();
    for (Message<BatchEvent> message : messages) {
      Long retries = getRetryCount(message);
      if (retries > 3) {
        sendToDlq(message);
        return;
      } else {
        BatchEvent event = message.getPayload();
        log.trace("Received message as a generic AMQP 'Message' wrapper: {}", event);
        EventArchiveEntry entity = createEventArchiveEntityFromMessage(event);
        events.add(entity);
        if (simulateException(false)) {
          throw new RuntimeException("Could not store message '" + event.getSerialNumber() + "'.");
        }
      }
    }
    eventArchiveEntryRepository.saveAll(events);
  }

  @RabbitListener(queues = BatchAmqpConfiguration.RETRY_QUEUE_NAME, containerFactory = "retryListenerContainerFactory")
  public void receiveDlqMessage(final Message<BatchEvent> message) {
    BatchEvent event = message.getPayload();
    log.trace("Received message as a generic AMQP 'Message' wrapper: {}", event);
    Long retries = getRetryCount(message);
    if (retries > 3) {
      sendToDlq(message);
    } else {
      if (simulateException(true)) {
        throw new RuntimeException(String.format("Could not store message '%s' after retry #%d.", event.getSerialNumber(), retries));
      }
      EventArchiveEntry entity = createEventArchiveEntityFromMessage(event);
      eventArchiveEntryRepository.save(entity);
    }
  }

  private EventArchiveEntry createEventArchiveEntityFromMessage(BatchEvent event) {
    return EventArchiveEntry.builder()
        .id(UUID.randomUUID().toString())
        .batchId(event.getBatchId())
        .serialNumber(event.getSerialNumber())
        .username(event.getUsername())
        .created(event.getCreatedOn().toInstant())
        .messageNumber(event.getMessageNumber())
        .isNew(true)
        .build();
  }

  private Long getRetryCount(Message<BatchEvent> in) {
    List<Map<String, ?>> xDeathHeaders = in.getHeaders().get("x-death", List.class);
    if (xDeathHeaders != null && !xDeathHeaders.isEmpty()) {
      return (Long) xDeathHeaders.get(0).get("count");
    }

    return 0L;
  }

  private void sendToDlq(Message<BatchEvent> failedMessage) {
    log.info("Retries exceeded for message '{}', putting into DLQ.", failedMessage.getPayload().getSerialNumber());
    this.rabbitMessagingTemplate.send(DEADLETTER_QUEUE_NAME, failedMessage);
  }

  private boolean simulateException(boolean isRetry) {
    var simulateException = false;
    val randomValue = random.nextDouble() * 100.0;
    if (isRetry) {
      simulateException = randomValue < exceptionProbabilityFailedMessage;
    } else {
      simulateException = randomValue < exceptionProbability;
    }
    if (log.isDebugEnabled() && simulateException) {
      log.info("Random value {} simulates an exception. Message already retried: {}.", randomValue, isRetry);
    }

    return simulateException;
  }

}
