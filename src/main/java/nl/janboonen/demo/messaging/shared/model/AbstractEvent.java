package nl.janboonen.demo.messaging.shared.model;

import java.io.Serializable;
import java.time.ZonedDateTime;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
public abstract class AbstractEvent implements Serializable {

  private long serialNumber;

  private ZonedDateTime createdOn;

  private String username;

}
