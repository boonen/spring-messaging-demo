package nl.janboonen.demo.messaging.shared.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.time.Instant;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.Persistable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "event_entries")
public class EventArchiveEntry implements Persistable<String>, Serializable {

  @Id
  private String id;

  @JsonIgnore
  private transient boolean isNew;

  private String username;

  private String batchId;

  private long serialNumber;

  private int messageNumber;

  private Instant created;

  @Override
  public boolean isNew() {
    return isNew;
  }

}
