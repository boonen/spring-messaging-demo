package nl.janboonen.demo.messaging.shared.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
public class BatchEvent extends AbstractEvent {

  private String batchId;

  private int messageNumber;

}
