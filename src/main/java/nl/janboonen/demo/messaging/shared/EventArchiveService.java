package nl.janboonen.demo.messaging.shared;

import java.util.List;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.janboonen.demo.messaging.shared.entity.EventArchiveEntry;
import nl.janboonen.demo.messaging.shared.entity.EventArchiveEntryRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@AllArgsConstructor
@Service
public class EventArchiveService {

  private final EventArchiveEntryRepository eventArchiveEntryRepository;

  @Transactional(readOnly = true)
  public List<EventArchiveEntry> getAllEventsFromArchive(String batchId) {
    return eventArchiveEntryRepository.findAllByBatchIdOrderByCreatedDesc(batchId)
        .collect(Collectors.toList());
  }

}
