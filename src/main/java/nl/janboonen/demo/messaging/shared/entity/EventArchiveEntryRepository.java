package nl.janboonen.demo.messaging.shared.entity;

import java.util.stream.Stream;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EventArchiveEntryRepository extends JpaRepository<EventArchiveEntry, String> {

  Stream<EventArchiveEntry> findAllByBatchIdOrderByCreatedDesc(String batchId);

}
